## Mumble Positional Audio for Valheim

**Not official or endorsed in any way by IronGate**

This is a BepInEx plugin that feeds Mumble character and camera data from the
game to use in its positional audio system.

[Join the Discord!][discord]

[discord]: https://discord.gg/mpXTshw276

### Installation

[Video walkthrough courtesy of Freedom.4x4](https://www.youtube.com/watch?v=6LcEDtsmIlM&feature=youtu.be)

#### r2modman / Thunderstore

Find the mod in "Online," click "Download." BepInEx should be installed as a
dependency. You're done!

It can also be found on [Thunderstore][thunderstore].

[thunderstore]: https://valheim.thunderstore.io/package/jrobsonchase/PositionalAudio/

#### Manual

First, you're going to need BepInEx for Valheim. If you've already installed
another mod that uses it, you can skip this step. You can get it
[here](https://valheim.thunderstore.io/package/denikson/BepInExPack_Valheim/).
Follow the instructions. Note: if you're using Linux, you'll need to follow the
extra Steam setup described
[here](https://bepinex.github.io/bepinex_docs/master/articles/advanced/steam_interop.html?tabs=tabid-1).

Once BepInEx is installed, head over to [Packages][packages] (or
[Thunderstore][thunderstore]) and grab the latest zip. Put the dll in
`Valheim/BepInEx/plugins`.

[packages]: https://gitlab.com/jrobsonchase/valheimpositionalaudio/-/packages

### Usage

First start Mumble. If you haven't already, enable positional audio in
Configure -> Settings -> Audio Output -> Interface. You also need to check
"Link to Game and Transmit position" in Configure -> Settings -> Plugins ->
Options.

When Valheim starts with BepInEx, you should see a terminal with
Unity/BepInEx logs. You should see a line that says `Mumble Link Loaded` when
the plugin gets initialized. It won't do anything else until you join a game.
Once your character loads, you should see `Mumble Shared Memory Initialized`
in the logs. In the Mumble messages panel on the left side, you should now
see "Valheim Linked."

Now, two people on the same server should hear each others' voices coming
from their characters' location. You can also tweak the Positional Audio
settings in Audio Output to make it more realistic with regard to volume
dropoff over distance. Setting Minimum Distance to 1m, Maximum Distance to
50m, and Minimum Volume to 0% works well in my experience.

#### Global Voice Key

This is disabled by default. It's a bit of a hack. [A Mumble Shout/Whisper
shortcut is much better.][shout]

[shout]: https://wiki.mumble.info/wiki/Mumbleguide/English#Whisper

By default, T is bound to "Global Voice." This changes your mumble context
such that your voice (and everyone else's from your perspective) are no
longer directional. Use it if you want a "viking walkie-talkie" of sorts. It
can be configured via the the file generated in `BepInEx/config/` file, or
with [BepInEx.ConfigurationManager][configmanager].

[configmanager]: https://github.com/BepInEx/BepInEx.ConfigurationManager

### Server-side

Note: This section is entirely optional and applies to the Mumble server, not
Valheim

If you want to be able to enforce that mumble channel membership is
contingent on having the game linked, give [mumla][mumla] a shot. It's a
fairly simple Mumble bot that moves people around based on whether or not
they have a game linked for positional audio. People who have a game linked
will be automatically put in the corresponding channel for their game/server,
and people who don't have one linked will automatically be removed from those
channels. Admins are immune.

**This will not work with the global voice key.** Because it changes your
*context, you'll just find yourself placed in a channel all by yourself, which
*is the exact opposite of what you want. Set up a real mumble keybind instead.

[mumla]: https://gitlab.com/jrobsonchase/mumla

### Troubleshooting

First, make sure BepInEx works. On Windows, you should get a terminal with
logs when you start Valheim. On Linux, you should see logs in
`BepInEx/LogOutput.log`. Next, make sure you see the startup message for
PositionalAudio.

If you have access to the mumble server and can enable grpc (might need a
development snapshot if you're running on Windows), try [this debugging
utility][debugtool] to verify the id/context/location for each player.

[debugtool]: https://gitlab.com/jrobsonchase/mumble-position-debug

If things still aren't working, toss me an issue and I'll see if I can help
fix it.
