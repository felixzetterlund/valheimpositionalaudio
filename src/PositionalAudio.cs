﻿using System.Linq;
using System;
using BepInEx;
using BepInEx.Configuration;
using UnityEngine;

namespace PositionalAudio
{

    [BepInPlugin("org.jrobsonchase.valheim.plugins.positionalaudio", "Mumble Positional Audio", Version.String)]
    public class PositionalAudio : BaseUnityPlugin
    {

        mumblelib.LinkFileManager fileManager;
        mumblelib.LinkFile mumbleLink;

        private string context;
        private bool inGlobal;

        private ConfigEntry<BepInEx.Configuration.KeyboardShortcut> globalVoice;
        private ConfigEntry<bool> globalVoiceEnabled;

        unsafe void Init()
        {
            mumbleLink = fileManager.Open();
            mumbleLink.Name = "Valheim";
            mumbleLink.Description = "Valheim Positional Audio";
            mumbleLink.UIVersion = 2;
            string id = randomString(16);
            UnityEngine.Debug.LogFormat("Setting Mumble ID to {0}", id);
            mumbleLink.ID = id;
            UnityEngine.Debug.LogFormat("Setting context to {0}", ZNet.instance.GetWorldName());
            context = ZNet.instance.GetWorldName();
            mumbleLink.Context = context;

            UnityEngine.Debug.Log("Mumble Shared Memory Initialized");
        }
        private static System.Random random = new System.Random();
        private string randomString(int len)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, len)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        private void enterGlobalContext()
        {
            if (!inGlobal)
            {
                // A gibberish context will make your voice non-positional to everyone else.
                UnityEngine.Debug.Log("Switching to global voice context");
                mumbleLink.Context = randomString(16);
                inGlobal = true;
            }
        }

        private void exitGlobalContext()
        {
            if (inGlobal)
            {
                UnityEngine.Debug.Log("Switching to local voice context");
                mumbleLink.Context = context;
                inGlobal = false;
            }
        }

        // Awake is called once when both the game and the plug-in are loaded
        void Awake()
        {
            globalVoiceEnabled = Config.Bind<bool>(
                "Global Voice",
                "enabled",
                false,
                "Global voice key enabled flag"
            );
            globalVoice = Config.Bind(
                "Global Voice",
                "key",
                new BepInEx.Configuration.KeyboardShortcut(KeyCode.T),
                "Global voice key. Note: Deprecated in favor of a Mumble \"Shout/Whisper\" shortcut"
            );
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                UnityEngine.Debug.Log("Windows Mumble Link Loaded");
                fileManager = new mumblelib.WindowsLinkFileManager();
            }
            else
            {
                UnityEngine.Debug.Log("Unix Mumble Link Loaded");
                fileManager = new mumblelib.UnixLinkFileManager();
            }
        }

        void Update()
        {
            if (mumbleLink != null)
            {
                if (globalVoiceEnabled.Value)
                {
                    bool pressed = Input.GetKey(globalVoice.Value.MainKey);

                    // Only check the mods if *entering* global, not exiting
                    // This way, you can release them after the initial press
                    if (!inGlobal)
                    {
                        foreach (KeyCode key in globalVoice.Value.Modifiers)
                        {
                            if (!Input.GetKey(key))
                            {
                                pressed = false;
                            }
                        }
                    }

                    if (pressed)
                    {
                        enterGlobalContext();
                    }
                    else
                    {
                        exitGlobalContext();
                    }
                }
            }
        }

        private static bool isValid(GameObject gameObject)
        {
            return gameObject != null && gameObject.transform != null;
        }

        private bool findGameObjects()
        {
            if (!inGame && ZDOMan.instance != null)
            {
                foreach (var gameObj in GameObject.FindObjectsOfType(typeof(Player)))
                {
                    Player somePlayer = (Player)gameObj;

                    if (ZDOMan.instance.GetMyID() == somePlayer.GetZDOID().userID)
                    {
                        characterObject = somePlayer.gameObject;
                    }
                }
                cameraObject = GameObject.Find("_GameMain/Main Camera/Listner");
            }

            if ((!isValid(characterObject)) ||
                (!isValid(cameraObject)) ||
                (ZNet.instance == null) ||
                (ZDOMan.instance == null))
            {
                if (inGame)
                {
                    UnityEngine.Debug.Log("Lost game objects");
                    inGame = false;
                }
                return false;
            }


            if (!inGame)
            {
                UnityEngine.Debug.Log("Found game objects");
                inGame = true;
            }
            return true;
        }

        private bool inGame = false;
        private GameObject characterObject;
        private GameObject cameraObject;


        private bool initBreaker = false;

        unsafe void FixedUpdate()
        {
            if (!findGameObjects())
            {
                if (mumbleLink != null)
                {
                    mumbleLink.Dispose();
                    mumbleLink = null;
                }
                return;
            }

            if (mumbleLink == null)
            {
                try
                {
                    Init();
                    initBreaker = false;
                }
                catch(Exception e)
                {
                    if(!initBreaker) {
                        initBreaker = true;
                        UnityEngine.Debug.LogFormat("error initializing mumble link: {0}", e.Message);
                    }

                    return;
                }
            }

            Transform camera = cameraObject.transform;
            Transform character = characterObject.transform;

            if (camera.position != null)
                mumbleLink.CameraPosition = camera.position;
            if (camera.forward != null)
                mumbleLink.CameraForward = camera.forward;
            if (character.position != null)
                mumbleLink.CharacterPosition = character.position;
            if (character.forward != null)
                mumbleLink.CharacterForward = character.forward;

            mumbleLink.Tick();
        }

        void OnDestroy()
        {
            if (mumbleLink != null)
            {
                mumbleLink.Dispose();
                mumbleLink = null;
            }
        }
    }
}
