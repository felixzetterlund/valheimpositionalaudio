#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR/..

mkdir -p pkg

dotnet build -c Release

cp bin/Release/net461/PositionalAudio.dll pkg/
envsubst < scripts/manifest.json.tmpl > pkg/manifest.json
cp README.md pkg/

cd pkg

zip PositionalAudio.zip *